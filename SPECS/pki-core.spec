################################################################################
Name:             pki-core
################################################################################

%global           product_name IDM PKI
%global           product_id idm-pki

Summary:          %{product_name} Package
URL:              https://www.dogtagpki.org
# The entire source code is GPLv2 except for 'pki-tps' which is LGPLv2
License:          GPLv2 and LGPLv2

# For development (i.e. unsupported) releases, use x.y.z-0.n.<phase>.
# For official (i.e. supported) releases, use x.y.z-r where r >=1.
%global           release_number 1
Version:          10.15.1
Release:          %{?release_number}%{?_timestamp}%{?_commit_id}%{?dist}
#global           _phase -alpha1

# To create a tarball from a version tag:
# $ git archive \
#     --format=tar.gz \
#     --prefix pki-<version>/ \
#     -o pki-<version>.tar.gz \
#     <version tag>
Source: https://github.com/dogtagpki/pki/archive/v%{version}%{?_phase}/pki-%{version}%{?_phase}.tar.gz

# To create a patch for all changes since a version tag:
# $ git format-patch \
#     --stdout \
#     <version tag> \
#     > pki-VERSION-RELEASE.patch
# Patch: pki-VERSION-RELEASE.patch

# md2man isn't available on i686. Additionally, we aren't generally multi-lib
# compatible (https://fedoraproject.org/wiki/Packaging:Java)
%if ! 0%{?rhel} || 0%{?rhel} >= 8
ExcludeArch: i686
%endif

################################################################################
# NSS
################################################################################

%global nss_default_db_type sql

################################################################################
# Python
################################################################################

%if 0%{?rhel} && 0%{?rhel} <= 8
%global python_executable /usr/libexec/platform-python
%else
%global python_executable /usr/bin/python3
%endif

################################################################################
# Java
################################################################################

%if 0%{?fedora} && 0%{?fedora} <= 32 || 0%{?rhel} && 0%{?rhel} <= 8
%define java_devel java-1.8.0-openjdk-devel
%define java_headless java-1.8.0-openjdk-headless
%define java_home /usr/lib/jvm/jre-1.8.0-openjdk
%else
%define java_devel java-11-openjdk-devel
%define java_headless java-11-openjdk-headless
%define java_home /usr/lib/jvm/jre-11-openjdk
%endif

################################################################################
# RESTEasy
################################################################################

%define jaxrs_api_jar /usr/share/java/jboss-jaxrs-2.0-api.jar
%define resteasy_lib /usr/share/java/resteasy

################################################################################
# PKI
################################################################################

# By default the build will execute unit tests unless --without test
# option is specified.

%bcond_without test

# By default all packages will be built except the ones specified with
# --without <package> option (exclusion method).

# If --with pkgs option is specified, only packages specified with
# --with <package> will be built (inclusion method).

# bcond_with pkgs
%global with_pkgs 1

# Define package_option macro to wrap bcond_with or bcond_without macro
# depending on package selection method.

%if %{with pkgs}
%define package_option() %bcond_with %1
%else
%define package_option() %bcond_without %1
%endif

# Define --with <package> or --without <package> options depending on
# package selection method.

# package_option base
%global with_base 1
# package_option server
%global with_server 1
# package_option acme
%global with_acme 1
# package_option ca
%global with_ca 1
# package_option kra
%global with_kra 1
# package_option ocsp
# package_option tks
# package_option tps
# package_option javadoc
# package_option console
# package_option theme
# package_option meta
# package_option tests
# package_option debug
%global with_debug 1

%if ! %{with debug}
%define debug_package %{nil}
%endif

# ignore unpackaged files from native 'tpsclient'
# REMINDER:  Remove this '%%define' once 'tpsclient' is rewritten as a Java app
%define _unpackaged_files_terminate_build 0

# The PKI UID and GID are preallocated, see:
# https://bugzilla.redhat.com/show_bug.cgi?id=476316
# https://bugzilla.redhat.com/show_bug.cgi?id=476782
# https://pagure.io/setup/blob/master/f/uidgid
# /usr/share/doc/setup/uidgid
%define pki_username pkiuser
%define pki_uid 17
%define pki_groupname pkiuser
%define pki_gid 17
%define pki_homedir /usr/share/pki

%global saveFileContext() \
if [ -s /etc/selinux/config ]; then \
     . %{_sysconfdir}/selinux/config; \
     FILE_CONTEXT=%{_sysconfdir}/selinux/%1/contexts/files/file_contexts; \
     if [ "${SELINUXTYPE}" == %1 -a -f ${FILE_CONTEXT} ]; then \
          cp -f ${FILE_CONTEXT} ${FILE_CONTEXT}.%{name}; \
     fi \
fi;

%global relabel() \
. %{_sysconfdir}/selinux/config; \
FILE_CONTEXT=%{_sysconfdir}/selinux/%1/contexts/files/file_contexts; \
selinuxenabled; \
if [ $? == 0  -a "${SELINUXTYPE}" == %1 -a -f ${FILE_CONTEXT}.%{name} ]; then \
     fixfiles -C ${FILE_CONTEXT}.%{name} restore; \
     rm -f ${FILE_CONTEXT}.%name; \
fi;

################################################################################
# Build Dependencies
################################################################################

BuildRequires:    make
BuildRequires:    cmake >= 3.0.2
BuildRequires:    gcc-c++
BuildRequires:    zip
BuildRequires:    %{java_devel}
BuildRequires:    javapackages-tools
BuildRequires:    redhat-rpm-config
BuildRequires:    ldapjdk >= 4.24.0, ldapjdk < 5.0.0
BuildRequires:    apache-commons-cli
BuildRequires:    apache-commons-codec
BuildRequires:    apache-commons-io
BuildRequires:    apache-commons-lang3 >= 3.2
BuildRequires:    apache-commons-logging
BuildRequires:    apache-commons-net
BuildRequires:    glassfish-jaxb-api
BuildRequires:    slf4j
BuildRequires:    slf4j-jdk14
BuildRequires:    nspr-devel
BuildRequires:    nss-devel >= 3.36.1

BuildRequires:    openldap-devel
BuildRequires:    pkgconfig
BuildRequires:    policycoreutils

BuildRequires:    python3-lxml
BuildRequires:    python3-sphinx

BuildRequires:    xalan-j2
BuildRequires:    xerces-j2

BuildRequires:    resteasy >= 3.0.26

BuildRequires:    python3 >= 3.5
BuildRequires:    python3-devel
BuildRequires:    python3-setuptools
BuildRequires:    python3-cryptography
BuildRequires:    python3-lxml
BuildRequires:    python3-ldap
BuildRequires:    python3-libselinux
BuildRequires:    python3-requests >= 2.6.0
BuildRequires:    python3-six

BuildRequires:    tomcat

BuildRequires:    junit
BuildRequires:    jpackage-utils >= 0:1.7.5-10
BuildRequires:    jss >= 4.11.0, jss < 5.0.0
BuildRequires:    tomcatjss >= 7.8.0, tomcatjss < 8.0.0

BuildRequires:    systemd-units

# additional build requirements needed to build native 'tpsclient'
# REMINDER:  Revisit these once 'tpsclient' is rewritten as a Java app
BuildRequires:    apr-devel
BuildRequires:    apr-util-devel
BuildRequires:    cyrus-sasl-devel
BuildRequires:    httpd-devel >= 2.4.2
BuildRequires:    pcre-devel
BuildRequires:    systemd
BuildRequires:    zlib
BuildRequires:    zlib-devel

# build dependency to build man pages
%if 0%{?fedora} && 0%{?fedora} <= 30 || 0%{?rhel} && 0%{?rhel} <= 8
BuildRequires:    go-md2man
%else
BuildRequires:    golang-github-cpuguy83-md2man
%endif

# pki-healthcheck depends on the following library
%if 0%{?rhel}
BuildRequires:    ipa-healthcheck-core
%else
BuildRequires:    freeipa-healthcheck-core
%endif

# PKICertImport depends on certutil and openssl
BuildRequires:    nss-tools
BuildRequires:    openssl

# description for top-level package (if there is a separate meta package)
%if "%{name}" != "%{product_id}"
%description

%{product_name} is an enterprise software system designed
to manage enterprise Public Key Infrastructure deployments.

%{product_name} consists of the following components:

  * Automatic Certificate Management Environment (ACME) Responder
  * Certificate Authority (CA)
  * Key Recovery Authority (KRA)
  * Online Certificate Status Protocol (OCSP) Manager
  * Token Key Service (TKS)
  * Token Processing Service (TPS)

%endif

%if %{with meta}
%if "%{name}" != "%{product_id}"
################################################################################
%package -n       %{product_id}
################################################################################

Summary:          %{product_name} Package
%endif

# Make certain that this 'meta' package requires the latest version(s)
# of ALL PKI theme packages
Requires:         %{product_id}-server-theme = %{version}-%{release}
Requires:         %{product_id}-console-theme = %{version}-%{release}

# Make certain that this 'meta' package requires the latest version(s)
# of ALL PKI core packages
Requires:         %{product_id}-acme = %{version}-%{release}
Requires:         %{product_id}-ca = %{version}-%{release}
Requires:         %{product_id}-kra = %{version}-%{release}
Requires:         %{product_id}-ocsp = %{version}-%{release}
Requires:         %{product_id}-tks = %{version}-%{release}
Requires:         %{product_id}-tps = %{version}-%{release}

# Make certain that this 'meta' package requires the latest version(s)
# of PKI console
Requires:         %{product_id}-console = %{version}-%{release}
Requires:         %{product_id}-javadoc = %{version}-%{release}

# Make certain that this 'meta' package requires the latest version(s)
# of ALL PKI clients -- except for s390/s390x where 'esc' is not built
%ifnarch s390 s390x
Requires:         esc >= 1.1.1
%endif

# description for top-level package (unless there is a separate meta package)
%if "%{name}" == "%{product_id}"
%description
%else
%description -n   %{product_id}
%endif

%{product_name} is an enterprise software system designed
to manage enterprise Public Key Infrastructure deployments.

%{product_name} consists of the following components:

  * Automatic Certificate Management Environment (ACME) Responder
  * Certificate Authority (CA)
  * Key Recovery Authority (KRA)
  * Online Certificate Status Protocol (OCSP) Manager
  * Token Key Service (TKS)
  * Token Processing Service (TPS)

# with meta
%endif

%if %{with base}
################################################################################
%package -n       %{product_id}-symkey
################################################################################

Summary:          %{product_name} Symmetric Key Package

Obsoletes:        pki-symkey < %{version}-%{release}
Provides:         pki-symkey = %{version}-%{release}

Requires:         %{java_headless}
Requires:         jpackage-utils >= 0:1.7.5-10
Requires:         jss >= 4.11.0, jss < 5.0.0
Requires:         nss >= 3.38.0

# Ensure we end up with a useful installation
Conflicts:        pki-symkey < %{version}
Conflicts:        pki-javadoc < %{version}
Conflicts:        pki-server-theme < %{version}
Conflicts:        pki-console-theme < %{version}

%description -n   %{product_id}-symkey
This package provides library for symmetric key operations.

################################################################################
%package -n       %{product_id}-base
################################################################################

Summary:          %{product_name} Base Package
BuildArch:        noarch

Obsoletes:        pki-base < %{version}-%{release}
Provides:         pki-base = %{version}-%{release}

Requires:         nss >= 3.36.1

Requires:         python3-pki = %{version}-%{release}
Requires(post):   python3-pki = %{version}-%{release}

# Ensure we end up with a useful installation
Conflicts:        pki-symkey < %{version}
Conflicts:        pki-javadoc < %{version}
Conflicts:        pki-server-theme < %{version}
Conflicts:        pki-console-theme < %{version}

%description -n   %{product_id}-base
This package provides default configuration files for %{product_name} client.

################################################################################
%package -n       python3-%{product_id}
################################################################################

Summary:          %{product_name} Python 3 Package
BuildArch:        noarch

Obsoletes:        python3-pki < %{version}-%{release}
Provides:         python3-pki = %{version}-%{release}

Obsoletes:        pki-base-python3 < %{version}-%{release}
Provides:         pki-base-python3 = %{version}-%{release}

%if 0%{?fedora} || 0%{?rhel} > 8
%{?python_provide:%python_provide python3-pki}
%endif

Requires:         %{product_id}-base = %{version}-%{release}
Requires:         python3 >= 3.5
Requires:         python3-cryptography
Requires:         python3-ldap
Requires:         python3-lxml
Requires:         python3-requests >= 2.6.0
Requires:         python3-six
%if 0%{?rhel} < 9 || 0%{?fedora} < 34
Recommends:       python3-nss
%endif

%description -n   python3-%{product_id}
This package provides common and client library for Python 3.

################################################################################
%package -n       %{product_id}-base-java
################################################################################

Summary:          %{product_name} Base Java Package
BuildArch:        noarch

Obsoletes:        pki-base-java < %{version}-%{release}
Provides:         pki-base-java = %{version}-%{release}

Requires:         %{java_headless}
Requires:         apache-commons-cli
Requires:         apache-commons-codec
Requires:         apache-commons-io
Requires:         apache-commons-lang3 >= 3.2
Requires:         apache-commons-logging
Requires:         apache-commons-net
Requires:         glassfish-jaxb-api
Requires:         slf4j
Requires:         slf4j-jdk14
Requires:         jpackage-utils >= 0:1.7.5-10
Requires:         jss >= 4.11.0, jss < 5.0.0
Requires:         ldapjdk >= 4.24.0, ldapjdk < 5.0.0
Requires:         %{product_id}-base = %{version}-%{release}

%if 0%{?rhel} && 0%{?rhel} <= 8
Requires:         resteasy >= 3.0.26
%else
Requires:         resteasy-client >= 3.0.17-1
Requires:         resteasy-jaxb-provider >= 3.0.17-1
Requires:         resteasy-core >= 3.0.17-1
Requires:         resteasy-jackson2-provider >= 3.0.17-1
%endif

%if 0%{?fedora} >= 33 || 0%{?rhel} > 8
Requires:         jaxb-impl >= 2.3.3
Requires:         jakarta-activation >= 1.2.2
%endif

Requires:         xalan-j2
Requires:         xerces-j2
Requires:         xml-commons-apis
Requires:         xml-commons-resolver

%description -n   %{product_id}-base-java
This package provides common and client libraries for Java.

################################################################################
%package -n       %{product_id}-tools
################################################################################

Summary:          %{product_name} Tools Package

Obsoletes:        pki-tools < %{version}-%{release}
Provides:         pki-tools = %{version}-%{release}

Requires:         openldap-clients
Requires:         nss-tools >= 3.36.1
Requires:         %{product_id}-base-java = %{version}-%{release}
Requires:         p11-kit-trust

# PKICertImport depends on certutil and openssl
Requires:         nss-tools
Requires:         openssl

%description -n   %{product_id}-tools
This package provides tools that can be used to help make
%{product_name} into a more complete and robust PKI solution.

# with base
%endif

%if %{with server}
################################################################################
%package -n       %{product_id}-server
################################################################################

Summary:          %{product_name} Server Package
BuildArch:        noarch

Obsoletes:        pki-server < %{version}-%{release}
Provides:         pki-server = %{version}-%{release}

Requires:         hostname

Requires:         policycoreutils
Requires:         procps-ng
Requires:         openldap-clients
Requires:         openssl
Requires:         %{product_id}-symkey = %{version}-%{release}
Requires:         %{product_id}-tools = %{version}-%{release}

Requires:         keyutils

Requires:         policycoreutils-python-utils

Requires:         python3-lxml
Requires:         python3-libselinux
Requires:         python3-policycoreutils

Requires:         selinux-policy-targeted >= 3.13.1-159

Requires:         tomcat

Requires:         sudo
Requires:         systemd
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units
Requires(pre):    shadow-utils
Requires:         tomcatjss >= 7.8.0, tomcatjss < 8.0.0

# pki-healthcheck depends on the following library
%if 0%{?rhel}
Requires:         ipa-healthcheck-core
%else
Requires:         freeipa-healthcheck-core
%endif

# https://pagure.io/freeipa/issue/7742
%if 0%{?rhel}
Conflicts:        ipa-server < 4.7.1
%else
Conflicts:        freeipa-server < 4.7.1
%endif

Provides:         bundled(js-backbone) = 1.4.0
Provides:         bundled(js-bootstrap) = 3.4.1
Provides:         bundled(js-jquery) = 3.5.1
Provides:         bundled(js-jquery-i18n-properties) = 1.2.7
Provides:         bundled(js-patternfly) = 3.59.2
Provides:         bundled(js-underscore) = 1.9.2

%description -n   %{product_id}-server
This package provides libraries and utilities needed by %{product_name} services.

# with server
%endif

%if %{with acme}
################################################################################
%package -n       %{product_id}-acme
################################################################################

Summary:          %{product_name} ACME Package
BuildArch:        noarch

Obsoletes:        pki-acme < %{version}-%{release}
Provides:         pki-acme = %{version}-%{release}

Requires:         %{product_id}-server = %{version}-%{release}

%description -n   %{product_id}-acme
%{product_name} ACME responder is a service that provides an automatic certificate
management via ACME v2 protocol defined in RFC 8555.

# with acme
%endif

%if %{with ca}
################################################################################
%package -n       %{product_id}-ca
################################################################################

Summary:          %{product_name} CA Package
BuildArch:        noarch

Obsoletes:        pki-ca < %{version}-%{release}
Provides:         pki-ca = %{version}-%{release}

Requires:         %{product_id}-server = %{version}-%{release}
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description -n   %{product_id}-ca
%{product_name} Certificate Authority (CA) is a required subsystem which issues,
renews, revokes, and publishes certificates as well as compiling and
publishing Certificate Revocation Lists (CRLs).

The Certificate Authority can be configured as a self-signing Certificate
Authority, where it is the root CA, or it can act as a subordinate CA,
where it obtains its own signing certificate from a public CA.

# with ca
%endif

%if %{with kra}
################################################################################
%package -n       %{product_id}-kra
################################################################################

Summary:          %{product_name} KRA Package
BuildArch:        noarch

Obsoletes:        pki-kra < %{version}-%{release}
Provides:         pki-kra = %{version}-%{release}

Requires:         %{product_id}-server = %{version}-%{release}
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description -n   %{product_id}-kra
%{product_name} Key Recovery Authority (KRA) is an optional subsystem that can act
as a key archival facility.  When configured in conjunction with the
Certificate Authority (CA), the KRA stores private encryption keys as part of
the certificate enrollment process.  The key archival mechanism is triggered
when a user enrolls in the PKI and creates the certificate request.  Using the
Certificate Request Message Format (CRMF) request format, a request is
generated for the user's private encryption key.  This key is then stored in
the KRA which is configured to store keys in an encrypted format that can only
be decrypted by several agents requesting the key at one time, providing for
protection of the public encryption keys for the users in the PKI deployment.

Note that the KRA archives encryption keys; it does NOT archive signing keys,
since such archival would undermine non-repudiation properties of signing keys.

# with kra
%endif

%if %{with ocsp}
################################################################################
%package -n       %{product_id}-ocsp
################################################################################

Summary:          %{product_name} OCSP Package
BuildArch:        noarch

Obsoletes:        pki-ocsp < %{version}-%{release}
Provides:         pki-ocsp = %{version}-%{release}

Requires:         %{product_id}-server = %{version}-%{release}
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description -n   %{product_id}-ocsp
%{product_name} Online Certificate Status Protocol (OCSP) Manager is an optional
subsystem that can act as a stand-alone OCSP service.  The OCSP Manager
performs the task of an online certificate validation authority by enabling
OCSP-compliant clients to do real-time verification of certificates.  Note
that an online certificate-validation authority is often referred to as an
OCSP Responder.

Although the Certificate Authority (CA) is already configured with an
internal OCSP service.  An external OCSP Responder is offered as a separate
subsystem in case the user wants the OCSP service provided outside of a
firewall while the CA resides inside of a firewall, or to take the load of
requests off of the CA.

The OCSP Manager can receive Certificate Revocation Lists (CRLs) from
multiple CA servers, and clients can query the OCSP Manager for the
revocation status of certificates issued by all of these CA servers.

When an instance of OCSP Manager is set up with an instance of CA, and
publishing is set up to this OCSP Manager, CRLs are published to it
whenever they are issued or updated.

# with ocsp
%endif

%if %{with tks}
################################################################################
%package -n       %{product_id}-tks
################################################################################

Summary:          %{product_name} TKS Package
BuildArch:        noarch

Obsoletes:        pki-tks < %{version}-%{release}
Provides:         pki-tks = %{version}-%{release}

Requires:         %{product_id}-server = %{version}-%{release}
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description -n   %{product_id}-tks
%{product_name} Token Key Service (TKS) is an optional subsystem that manages the
master key(s) and the transport key(s) required to generate and distribute
keys for hardware tokens.  TKS provides the security between tokens and an
instance of Token Processing System (TPS), where the security relies upon the
relationship between the master key and the token keys.  A TPS communicates
with a TKS over SSL using client authentication.

TKS helps establish a secure channel (signed and encrypted) between the token
and the TPS, provides proof of presence of the security token during
enrollment, and supports key changeover when the master key changes on the
TKS.  Tokens with older keys will get new token keys.

Because of the sensitivity of the data that TKS manages, TKS should be set up
behind the firewall with restricted access.

# with tks
%endif

%if %{with tps}
################################################################################
%package -n       %{product_id}-tps
################################################################################

Summary:          %{product_name} TPS Package

Obsoletes:        pki-tps < %{version}-%{release}
Provides:         pki-tps = %{version}-%{release}

Requires:         %{product_id}-server = %{version}-%{release}
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

# additional runtime requirements needed to run native 'tpsclient'
# REMINDER:  Revisit these once 'tpsclient' is rewritten as a Java app

Requires:         nss-tools >= 3.36.1
Requires:         openldap-clients

%description -n   %{product_id}-tps
%{product_name} Token Processing System (TPS) is an optional subsystem that acts
as a Registration Authority (RA) for authenticating and processing
enrollment requests, PIN reset requests, and formatting requests from
the Enterprise Security Client (ESC).

TPS is designed to communicate with tokens that conform to
Global Platform's Open Platform Specification.

TPS communicates over SSL with various PKI backend subsystems (including
the Certificate Authority (CA), the Key Recovery Authority (KRA), and the
Token Key Service (TKS)) to fulfill the user's requests.

TPS also interacts with the token database, an LDAP server that stores
information about individual tokens.

The utility "tpsclient" is a test tool that interacts with TPS.  This
tool is useful to test TPS server configs without risking an actual
smart card.

# with tps
%endif

%if %{with javadoc}
################################################################################
%package -n       %{product_id}-javadoc
################################################################################

Summary:          %{product_name} Javadoc Package
BuildArch:        noarch

Obsoletes:        pki-javadoc < %{version}-%{release}
Provides:         pki-javadoc = %{version}-%{release}

# Ensure we end up with a useful installation
Conflicts:        pki-base < %{version}
Conflicts:        pki-symkey < %{version}
Conflicts:        pki-server-theme < %{version}
Conflicts:        pki-console-theme < %{version}

%description -n   %{product_id}-javadoc
This package provides %{product_name} API documentation.

# with javadoc
%endif

%if %{with console}
################################################################################
%package -n       %{product_id}-console
################################################################################

Summary:          %{product_name} Console Package
BuildArch:        noarch

Obsoletes:        pki-console < %{version}-%{release}
Provides:         pki-console = %{version}-%{release}

BuildRequires:    idm-console-framework >= 1.4.0, idm-console-framework < 2.0.0

Requires:         idm-console-framework >= 1.4.0, idm-console-framework < 2.0.0
Requires:         %{product_id}-base-java = %{version}-%{release}
Requires:         %{product_id}-console-theme = %{version}-%{release}

%description -n   %{product_id}-console
%{product_name} Console is a Java application used to administer %{product_name} Server.

# with console
%endif

%if %{with theme}
################################################################################
%package -n       %{product_id}-server-theme
################################################################################

Summary:          %{product_name} Server Theme Package
BuildArch:        noarch

Obsoletes:        pki-server-theme < %{version}-%{release}
Provides:         pki-server-theme = %{version}-%{release}

# Ensure we end up with a useful installation
Conflicts:        pki-base < %{version}
Conflicts:        pki-symkey < %{version}
Conflicts:        pki-console-theme < %{version}
Conflicts:        pki-javadoc < %{version}

%description -n   %{product_id}-server-theme
This package provides theme files for %{product_name} Server.

################################################################################
%package -n       %{product_id}-console-theme
################################################################################

Summary:          %{product_name} Console Theme Package
BuildArch:        noarch

Obsoletes:        pki-console-theme < %{version}-%{release}
Provides:         pki-console-theme = %{version}-%{release}

# Ensure we end up with a useful installation
Conflicts:        pki-base < %{version}
Conflicts:        pki-symkey < %{version}
Conflicts:        pki-server-theme < %{version}
Conflicts:        pki-javadoc < %{version}

%description -n   %{product_id}-console-theme
This package provides theme files for %{product_name} Console.

# with theme
%endif

%if %{with tests}
################################################################################
%package -n       %{product_id}-tests
################################################################################

Summary:          %{product_name} Tests
BuildArch:        noarch

Obsoletes:        pki-tests < %{version}-%{release}
Provides:         pki-tests = %{version}-%{release}

Requires:         python3-pylint
Requires:         python3-flake8

%description -n   %{product_id}-tests
This package provides test suite for %{product_name}.

# with tests
%endif

################################################################################
%prep
################################################################################

%autosetup -n pki-%{version}%{?_phase} -p 1

################################################################################
%build
################################################################################

# get Java <major>.<minor> version number
java_version=`%{java_home}/bin/java -XshowSettings:properties -version 2>&1 | sed -n 's/ *java.version *= *\([0-9]\+\.[0-9]\+\).*/\1/p'`

# if <major> == 1, get <minor> version number
# otherwise get <major> version number
java_version=`echo $java_version | sed -e 's/^1\.//' -e 's/\..*$//'`

# assume tomcat app_server
app_server=tomcat-9.0

%if 0%{?rhel} && 0%{?rhel} <= 8
%{__mkdir_p} build
cd build
%endif

%cmake \
    --no-warn-unused-cli \
    -DVERSION=%{version}-%{release} \
    -DVAR_INSTALL_DIR:PATH=/var \
    -DP11_KIT_TRUST=/etc/alternatives/libnssckbi.so.%{_arch} \
    -DJAVA_VERSION=${java_version} \
    -DJAVA_HOME=%{java_home} \
    -DPKI_JAVA_PATH=%{java_home}/bin/java \
    -DJAVA_LIB_INSTALL_DIR=%{_jnidir} \
    -DSYSTEMD_LIB_INSTALL_DIR=%{_unitdir} \
    -DAPP_SERVER=$app_server \
    -DJAXRS_API_JAR=%{jaxrs_api_jar} \
    -DRESTEASY_LIB=%{resteasy_lib} \
    -DNSS_DEFAULT_DB_TYPE=%{nss_default_db_type} \
    -DBUILD_PKI_CORE:BOOL=ON \
    -DPYTHON_EXECUTABLE=%{python_executable} \
%if ! %{with server} && ! %{with acme} && ! %{with ca} && ! %{with kra} && ! %{with ocsp} && ! %{with tks} && ! %{with tps}
    -DWITH_SERVER:BOOL=OFF \
%endif
    -DWITH_CA:BOOL=%{?with_ca:ON}%{!?with_ca:OFF} \
    -DWITH_KRA:BOOL=%{?with_kra:ON}%{!?with_kra:OFF} \
    -DWITH_OCSP:BOOL=%{?with_ocsp:ON}%{!?with_ocsp:OFF} \
    -DWITH_TKS:BOOL=%{?with_tks:ON}%{!?with_tks:OFF} \
    -DWITH_TPS:BOOL=%{?with_tps:ON}%{!?with_tps:OFF} \
    -DWITH_ACME:BOOL=%{?with_acme:ON}%{!?with_acme:OFF} \
    -DWITH_JAVADOC:BOOL=%{?with_javadoc:ON}%{!?with_javadoc:OFF} \
    -DWITH_TEST:BOOL=%{?with_test:ON}%{!?with_test:OFF} \
    -DBUILD_PKI_CONSOLE:BOOL=%{?with_console:ON}%{!?with_console:OFF} \
    -DTHEME=%{?with_theme:%{theme}} \
%if 0%{?rhel} && 0%{?rhel} <= 8
    ..
%else
    -B %{_vpath_builddir}
%endif

%if 0%{?fedora} || 0%{?rhel} > 8
cd %{_vpath_builddir}
%endif

# Do not use _smp_mflags to preserve build order
%{__make} \
    VERBOSE=%{?_verbose} \
    CMAKE_NO_VERBOSE=1 \
    DESTDIR=%{buildroot} \
    INSTALL="install -p" \
    --no-print-directory \
    all

################################################################################
%install
################################################################################

%if 0%{?rhel} && 0%{?rhel} <= 8
cd build
%else
cd %{_vpath_builddir}
%endif

%{__make} \
    VERBOSE=%{?_verbose} \
    CMAKE_NO_VERBOSE=1 \
    DESTDIR=%{buildroot} \
    INSTALL="install -p" \
    --no-print-directory \
    install

%if %{with test}
ctest --output-on-failure
%endif

%if %{with meta}
%{__mkdir_p} %{buildroot}%{_datadir}/doc/pki

cat > %{buildroot}%{_datadir}/doc/pki/README << EOF
This package is a "meta-package" whose dependencies pull in all of the
packages comprising the %{product_name} Suite.
EOF

# with meta
%endif

# Customize client library links in /usr/share/pki/lib
ln -sf /usr/share/java/jboss-logging/jboss-logging.jar %{buildroot}%{_datadir}/pki/lib/jboss-logging.jar
%if 0%{?fedora} && 0%{?fedora} <= 34 || 0%{?rhel} && 0%{?rhel} <= 8
ln -sf /usr/share/java/jboss-annotations-1.2-api/jboss-annotations-api_1.2_spec.jar %{buildroot}%{_datadir}/pki/lib/jboss-annotations-api_1.2_spec.jar
%else
ln -sf /usr/share/java/jakarta-annotations/jakarta.annotation-api.jar %{buildroot}%{_datadir}/pki/lib/jakarta.annotation-api.jar
%endif

%if %{with server}

# Customize server common library links in /usr/share/pki/server/common/lib
ln -sf %{jaxrs_api_jar} %{buildroot}%{_datadir}/pki/server/common/lib/jboss-jaxrs-2.0-api.jar
ln -sf /usr/share/java/jboss-logging/jboss-logging.jar %{buildroot}%{_datadir}/pki/server/common/lib/jboss-logging.jar
%if 0%{?fedora} && 0%{?fedora} <= 34 || 0%{?rhel} && 0%{?rhel} <= 8
ln -sf /usr/share/java/jboss-annotations-1.2-api/jboss-annotations-api_1.2_spec.jar %{buildroot}%{_datadir}/pki/server/common/lib/jboss-annotations-api_1.2_spec.jar
%else
ln -sf /usr/share/java/jakarta-annotations/jakarta.annotation-api.jar %{buildroot}%{_datadir}/pki/server/common/lib/jakarta.annotation-api.jar
%endif

# with server
%endif

%if %{with server}

%pre -n %{product_id}-server
getent group %{pki_groupname} >/dev/null || groupadd -f -g %{pki_gid} -r %{pki_groupname}
if ! getent passwd %{pki_username} >/dev/null ; then
    useradd -r -u %{pki_uid} -g %{pki_groupname} -d %{pki_homedir} -s /sbin/nologin -c "Certificate System" %{pki_username}
fi
exit 0

# with server
%endif

%if %{with base}

%post -n %{product_id}-base

if [ $1 -eq 1 ]
then
    # On RPM installation create system upgrade tracker
    echo "Configuration-Version: %{version}" > %{_sysconfdir}/pki/pki.version

else
    # On RPM upgrade run system upgrade
    echo "Upgrading PKI system configuration at `/bin/date`." >> /var/log/pki/pki-upgrade-%{version}.log
    /sbin/pki-upgrade 2>&1 | tee -a /var/log/pki/pki-upgrade-%{version}.log
    echo >> /var/log/pki/pki-upgrade-%{version}.log
fi

%postun -n %{product_id}-base

if [ $1 -eq 0 ]
then
    # On RPM uninstallation remove system upgrade tracker
    rm -f %{_sysconfdir}/pki/pki.version
fi

# with base
%endif

%if %{with server}

%post -n %{product_id}-server
# CVE-2021-3551
# Remove world access from existing installation logs
find /var/log/pki -maxdepth 1 -type f -exec chmod o-rwx {} \;

# Reload systemd daemons on upgrade only
if [ "$1" == "2" ]
then
    systemctl daemon-reload
fi

## preun -n pki-server
## NOTE:  At this time, NO attempt has been made to update ANY PKI subsystem
##        from EITHER 'sysVinit' OR previous 'systemd' processes to the new
##        PKI deployment process


## postun -n pki-server
## NOTE:  At this time, NO attempt has been made to update ANY PKI subsystem
##        from EITHER 'sysVinit' OR previous 'systemd' processes to the new
##        PKI deployment process

# with server
%endif

%if %{with meta}
%if "%{name}" != "%{product_id}"
################################################################################
%files -n %{product_id}
################################################################################
%else
%files
%endif

%doc %{_datadir}/doc/pki/README

# with meta
%endif

%if %{with base}
################################################################################
%files -n %{product_id}-symkey
################################################################################

%license base/symkey/LICENSE
%{_jnidir}/symkey.jar
%{_libdir}/symkey/

################################################################################
%files -n %{product_id}-base
################################################################################

%license base/common/LICENSE
%license base/common/LICENSE.LESSER
%doc %{_datadir}/doc/pki-base/html
%dir %{_datadir}/pki
%{_datadir}/pki/VERSION
%{_datadir}/pki/pom.xml
%dir %{_datadir}/pki/etc
%{_datadir}/pki/etc/pki.conf
%{_datadir}/pki/etc/logging.properties
%dir %{_datadir}/pki/lib
%dir %{_datadir}/pki/scripts
%{_datadir}/pki/scripts/config
%{_datadir}/pki/upgrade/
%{_datadir}/pki/key/templates
%dir %{_sysconfdir}/pki
%config(noreplace) %{_sysconfdir}/pki/pki.conf
%dir %{_localstatedir}/log/pki
%{_sbindir}/pki-upgrade
%{_mandir}/man1/pki-python-client.1.gz
%{_mandir}/man5/pki-logging.5.gz
%{_mandir}/man8/pki-upgrade.8.gz

################################################################################
%files -n %{product_id}-base-java
################################################################################

%license base/common/LICENSE
%license base/common/LICENSE.LESSER
%{_datadir}/pki/examples/java/
%{_datadir}/pki/lib/*.jar
%dir %{_javadir}/pki
%{_javadir}/pki/pki-cmsutil.jar
%{_javadir}/pki/pki-certsrv.jar

################################################################################
%files -n python3-%{product_id}
################################################################################

%license base/common/LICENSE
%license base/common/LICENSE.LESSER
%if %{with server}
%exclude %{python3_sitelib}/pki/server
%endif
%{python3_sitelib}/pki

################################################################################
%files -n %{product_id}-tools
################################################################################

%license base/tools/LICENSE
%doc base/tools/doc/README
%{_bindir}/p7tool
%{_bindir}/p12tool
%{_bindir}/pistool
%{_bindir}/pki
%{_bindir}/revoker
%{_bindir}/setpin
%{_bindir}/sslget
%{_bindir}/tkstool
%{_bindir}/AtoB
%{_bindir}/AuditVerify
%{_bindir}/BtoA
%{_bindir}/CMCEnroll
%{_bindir}/CMCRequest
%{_bindir}/CMCResponse
%{_bindir}/CMCRevoke
%{_bindir}/CMCSharedToken
%{_bindir}/CRMFPopClient
%{_bindir}/DRMTool
%{_bindir}/ExtJoiner
%{_bindir}/GenExtKeyUsage
%{_bindir}/GenIssuerAltNameExt
%{_bindir}/GenSubjectAltNameExt
%{_bindir}/HttpClient
%{_bindir}/KRATool
%{_bindir}/OCSPClient
%{_bindir}/PKCS10Client
%{_bindir}/PKCS12Export
%{_bindir}/PKICertImport
%{_bindir}/PrettyPrintCert
%{_bindir}/PrettyPrintCrl
%{_bindir}/TokenInfo
%{_javadir}/pki/pki-tools.jar
%{_datadir}/pki/tools/
%{_datadir}/pki/lib/p11-kit-trust.so
%{_mandir}/man1/AtoB.1.gz
%{_mandir}/man1/AuditVerify.1.gz
%{_mandir}/man1/BtoA.1.gz
%{_mandir}/man1/CMCEnroll.1.gz
%{_mandir}/man1/CMCRequest.1.gz
%{_mandir}/man1/CMCSharedToken.1.gz
%{_mandir}/man1/CMCResponse.1.gz
%{_mandir}/man1/DRMTool.1.gz
%{_mandir}/man1/KRATool.1.gz
%{_mandir}/man1/PrettyPrintCert.1.gz
%{_mandir}/man1/PrettyPrintCrl.1.gz
%{_mandir}/man1/pki.1.gz
%{_mandir}/man1/pki-audit.1.gz
%{_mandir}/man1/pki-ca-cert.1.gz
%{_mandir}/man1/pki-ca-kraconnector.1.gz
%{_mandir}/man1/pki-ca-profile.1.gz
%{_mandir}/man1/pki-client.1.gz
%{_mandir}/man1/pki-group.1.gz
%{_mandir}/man1/pki-group-member.1.gz
%{_mandir}/man1/pki-kra-key.1.gz
%{_mandir}/man1/pki-pkcs12-cert.1.gz
%{_mandir}/man1/pki-pkcs12-key.1.gz
%{_mandir}/man1/pki-pkcs12.1.gz
%{_mandir}/man1/pki-securitydomain.1.gz
%{_mandir}/man1/pki-tps-profile.1.gz
%{_mandir}/man1/pki-user.1.gz
%{_mandir}/man1/pki-user-cert.1.gz
%{_mandir}/man1/pki-user-membership.1.gz
%{_mandir}/man1/PKCS10Client.1.gz
%{_mandir}/man1/PKICertImport.1.gz

# with base
%endif

%if %{with server}
################################################################################
%files -n %{product_id}-server
################################################################################

%license base/common/THIRD_PARTY_LICENSES
%license base/server/LICENSE
%doc base/server/README
%attr(755,-,-) %dir %{_sysconfdir}/sysconfig/pki
%attr(755,-,-) %dir %{_sysconfdir}/sysconfig/pki/tomcat
%{_sbindir}/pkispawn
%{_sbindir}/pkidestroy
%{_sbindir}/pki-server
%{_sbindir}/pki-server-upgrade
%{_sbindir}/pki-healthcheck
%{python3_sitelib}/pki/server/
%{python3_sitelib}/pkihealthcheck-*.egg-info/
%config(noreplace) %{_sysconfdir}/pki/healthcheck.conf

%{_datadir}/pki/etc/tomcat.conf
%dir %{_datadir}/pki/deployment
%{_datadir}/pki/deployment/config/
%{_datadir}/pki/scripts/operations
%{_bindir}/pkidaemon
%{_bindir}/pki-server-nuxwdog
%dir %{_sysconfdir}/systemd/system/pki-tomcatd.target.wants
%attr(644,-,-) %{_unitdir}/pki-tomcatd@.service
%attr(644,-,-) %{_unitdir}/pki-tomcatd.target
%dir %{_sysconfdir}/systemd/system/pki-tomcatd-nuxwdog.target.wants
%attr(644,-,-) %{_unitdir}/pki-tomcatd-nuxwdog@.service
%attr(644,-,-) %{_unitdir}/pki-tomcatd-nuxwdog.target
%{_javadir}/pki/pki-cms.jar
%{_javadir}/pki/pki-cmsbundle.jar
%{_javadir}/pki/pki-tomcat.jar
%dir %{_sharedstatedir}/pki
%{_mandir}/man1/pkidaemon.1.gz
%{_mandir}/man5/pki_default.cfg.5.gz
%{_mandir}/man5/pki_healthcheck.conf.5.gz
%{_mandir}/man5/pki-server-logging.5.gz
%{_mandir}/man8/pki-server-upgrade.8.gz
%{_mandir}/man8/pkidestroy.8.gz
%{_mandir}/man8/pkispawn.8.gz
%{_mandir}/man8/pki-server.8.gz
%{_mandir}/man8/pki-server-acme.8.gz
%{_mandir}/man8/pki-server-instance.8.gz
%{_mandir}/man8/pki-server-subsystem.8.gz
%{_mandir}/man8/pki-server-nuxwdog.8.gz
%{_mandir}/man8/pki-server-migrate.8.gz
%{_mandir}/man8/pki-server-cert.8.gz
%{_mandir}/man8/pki-server-ca.8.gz
%{_mandir}/man8/pki-server-kra.8.gz
%{_mandir}/man8/pki-server-ocsp.8.gz
%{_mandir}/man8/pki-server-tks.8.gz
%{_mandir}/man8/pki-server-tps.8.gz
%{_mandir}/man8/pki-healthcheck.8.gz
%{_datadir}/pki/setup/
%{_datadir}/pki/server/

# with server
%endif

%if %{with acme}
################################################################################
%files -n %{product_id}-acme
################################################################################

%{_javadir}/pki/pki-acme.jar
%{_datadir}/pki/acme/

# with acme
%endif

%if %{with ca}
################################################################################
%files -n %{product_id}-ca
################################################################################

%license base/ca/LICENSE
%{_javadir}/pki/pki-ca.jar
%{_datadir}/pki/ca/

# with ca
%endif

%if %{with kra}
################################################################################
%files -n %{product_id}-kra
################################################################################

%license base/kra/LICENSE
%{_javadir}/pki/pki-kra.jar
%{_datadir}/pki/kra/

# with kra
%endif

%if %{with ocsp}
################################################################################
%files -n %{product_id}-ocsp
################################################################################

%license base/ocsp/LICENSE
%{_javadir}/pki/pki-ocsp.jar
%{_datadir}/pki/ocsp/

# with ocsp
%endif

%if %{with tks}
################################################################################
%files -n %{product_id}-tks
################################################################################

%license base/tks/LICENSE
%{_javadir}/pki/pki-tks.jar
%{_datadir}/pki/tks/

# with tks
%endif

%if %{with tps}
################################################################################
%files -n %{product_id}-tps
################################################################################

%license base/tps/LICENSE
%{_javadir}/pki/pki-tps.jar
%{_datadir}/pki/tps/
%{_mandir}/man5/pki-tps-connector.5.gz
%{_mandir}/man5/pki-tps-profile.5.gz
%{_mandir}/man1/tpsclient.1.gz

# files for native 'tpsclient'
# REMINDER:  Remove this comment once 'tpsclient' is rewritten as a Java app

%{_bindir}/tpsclient
%{_libdir}/tps/libtps.so
%{_libdir}/tps/libtokendb.so

# with tps
%endif

%if %{with javadoc}
################################################################################
%files -n %{product_id}-javadoc
################################################################################

%{_javadocdir}/pki-%{version}/

# with javadoc
%endif

%if %{with console}
################################################################################
%files -n %{product_id}-console
################################################################################

%license base/console/LICENSE
%{_bindir}/pkiconsole
%{_javadir}/pki/pki-console.jar

# with console
%endif

%if %{with theme}
################################################################################
%files -n %{product_id}-server-theme
################################################################################

%license themes/%{theme}/common-ui/LICENSE
%dir %{_datadir}/pki
%{_datadir}/pki/CS_SERVER_VERSION
%{_datadir}/pki/common-ui/
%{_datadir}/pki/server/webapps/pki/ca
%{_datadir}/pki/server/webapps/pki/css
%{_datadir}/pki/server/webapps/pki/esc
%{_datadir}/pki/server/webapps/pki/fonts
%{_datadir}/pki/server/webapps/pki/images
%{_datadir}/pki/server/webapps/pki/kra
%{_datadir}/pki/server/webapps/pki/ocsp
%{_datadir}/pki/server/webapps/pki/pki.properties
%{_datadir}/pki/server/webapps/pki/tks

################################################################################
%files -n %{product_id}-console-theme
################################################################################

%license themes/%{theme}/console-ui/LICENSE
%{_javadir}/pki/pki-console-theme.jar

# with theme
%endif

%if %{with tests}
################################################################################
%files -n %{product_id}-tests
################################################################################

%{_datadir}/pki/tests/

# with tests
%endif

################################################################################
%changelog
* Fri Jun 14 2024 Red Hat PKI Team <rhcs-maint@redhat.com> 10.15.1-1
- Rebase to PKI 10.15.1
- Fix CVE 2023-4727

* Thu Feb 08 2024 Red Hat PKI Team <rhcs-maint@redhat.com> 10.15.0-1
- Rebase to PKI 10.15.0

* Tue Jan 16 2024 Red Hat PKI Team <rhcs-maint@redhat.com> 10.15.0-0.1
- Rebase to PKI 10.15.0-alpha1

* Mon Jan 15 2024 Red Hat PKI Team <rhcs-maint@redhat.com> 10.14.3-2
- Replace pki-servlet-engine with tomcat

* Fri Feb 03 2023 Red Hat PKI Team <rhcs-maint@redhat.com> 10.14.3-1
- Rebase to PKI 10.14.3
- Bug 1959057 - An error has ocorred (IPA Error 4301:CertificateOperationError)
- Bug 2016164 - IdM Install fails on RHEL 8.5 Beta when DISA STIG is applied
- Bug 2022561 - ipa-healthcheck CADogtagCertsConfigCheck fail to process the scenario of renewed IPA CA certificates ( ipa get_cert_from_db() )

* Tue Nov 29 2022 Red Hat PKI Team <rhcs-maint@redhat.com> 10.14.2-1
- Rebase to PKI 10.14.2
- Bug 2149253 - Rebase to upstream version v2.14.2

* Mon Jul 25 2022 Red Hat PKI Team <rhcs-maint@redhat.com> 10.12.0-4
- Bug 2107334 - CVE-2022-2414 access to external entities when parsing XML can lead to XXE
- Rename packages to idm-pki

* Wed Jun 01 2022 Red Hat PKI Team <rhcs-maint@redhat.com> 10.12.0-3
- ExcludeArch i686 as md2man not available in RHEL 8.7

* Thu Feb 03 2022 Red Hat PKI Team <rhcs-maint@redhat.com> 10.12.0-2
- Bug 2027470 - pki-healthcheck ClonesConnectivyAndDataCheck fails

* Tue Nov 09 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.12.0-0.1
- Rebase to PKI 10.12.0
- Bug 1904112 - pki fails to start if empty dir /var/lib/pki/pki-tomcat/kra exists
- Bug 1984455 - [RFE] Date Format on the TPS Agent Page
- Bug 1980378 - 'keyctl_search: Required key not available' message when running 'ipa-healthcheck'
- Bug 2004084 - Reinstall of the same ipa-replica fails with 'RuntimeError: CA configuration failed.'
- Bug 2006070 - Upgrades incorrectly add secret attribute to connectors

* Thu Aug 12 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.11.0-2
- Bug 1992337 - Double issuance of non-CA subsystem certs at installation

* Mon Jul 26 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.11.0-1
- Rebase to PKI 10.11.0

* Mon Jun 14 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.11.0-0.3
- Rebase to PKI 10.11.0-alpha3

* Thu Jun 03 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.11.0-0.2
- Fix JAVA_HOME

* Wed Jun 02 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.11.0-0.1
- Rebase to PKI 10.11.0-alpha2

* Mon Feb 08 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.10.4-1
- Rebase to PKI 10.10.4
- Bug 1664435 - Error instantiating class for challenge_password with SCEP request
- Bug 1912418 - OCSP and TKS cloning failed due to duplicate replica ID
- Bug 1916686 - Memory leak during ACME performance test
- Bug 1919282 - ACME cert enrollment failed with HTTP 500

* Thu Jan 14 2021 Red Hat PKI Team <rhcs-maint@redhat.com> 10.10.3-1
- Rebase to PKI 10.10.3
- Bug 1584550 - CRMFPopClient: unexpected behavior with -y option when values are specified
- Bug 1590942 - CMCResponse treats -d as optional
- Bug 1890639 - Two-step installation with external certificates fails on HSM configured system
- Bug 1912493 - pkispawn reports incorrect FIPS mode

* Tue Dec 08 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.10.2-1
- Rebase to PKI 10.10.2
- Bug 1392616 - KRA key recovery cli kra-key-retrieve generates an invalid p12 file
- Bug 1897120 - pki-server cert-fix command failing
- Bug 1694664 - ipa: ERROR: Certificate operation cannot be completed: Unable to communicate with CMS (503)

* Tue Nov 17 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.10.1-1
- Rebase to PKI 10.10.1
- Bug 1843416 - kra-audit-mod fail with Invalid event configuration
- Bug 1889691 - ACME failed when run with more than 1 thread/connection
- Bug 1891577 - Sub-ordinate installation is failing with NullPointerException

* Wed Oct 28 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.10.0-1
- Rebase to PKI 10.10.0
- Add workaround for missing capture_output in Python 3.6
- Fix JSS initialization in pki-server <subsystem>-user-cert-add
- Fix NPE in UGSubsystem.findUsersByKeyword()
- Bug 1787115 - Need Method to copy SKI from CSR to Certificate signed
- Bug 1875563 - Add KRA Transport and Storage Certificates profiles, audit for IPA
- Bug 1883996 - Inconsistent folders in pki-tools

* Tue Oct 20 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.10.0-0.2.beta1
- Rebase to PKI 10.10.0-beta1
- Bug 1868233 - Disabling AIA and cert policy extensions in ACME examples

* Fri Sep 11 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.4-1
- Rebase to PKI 10.9.4
- Bug 1873235 - Fix SSL_ERROR_INAPPROPRIATE_FALLBACK_ALERT in pki ca-user-cert-add

* Thu Sep 03 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.3-1
- Rebase to PKI 10.9.3
- Bug 1869893 - Common certificates are missing in CS.cfg on shared PKI instance

* Tue Aug 18 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.2-2
- Bug 1871064 - Replica install failing during pki-ca component configuration

* Tue Aug 18 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.2-1
- Rebase to PKI 10.9.2

* Wed Aug 12 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.1-2
- Bug 1857933 - CA Installation is failing with ncipher v12.30 HSM
- Bug 1868233 - Disabling AIA and cert policy extensions in ACME examples

* Thu Aug 06 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.1-1
- Rebase to PKI 10.9.1
- Bug 1426572 - Fix Secure connection issue when server is down

* Fri Jul 31 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.0-1
- Rebase to PKI 10.9.0

* Tue Jul 14 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.0-0.7
- Fix pki kra-key-generate failure
- Fix error handling in PKIRealm

* Fri Jul 10 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.0-0.6
- Rebase to PKI 10.9.0-b4

* Thu Jun 25 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.0-0.4
- Rebase to PKI 10.9.0-b2

* Mon Jun 22 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.0-0.3
- Rebase to PKI 10.9.0-b1

* Tue May 26 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.9.0-0.1
- Rebase to PKI 10.9.0-a1

* Tue Mar 03 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.3-1
- Rebase to PKI 10.8.3
- Bug 1809210 - TPS installation failure on HSM machine
- Bug 1807421 - Subordinate CA installation failed
- Bug 1806840 - KRA cloning with HSM failed

* Wed Feb 19 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.2-2
- Bug 1795215 - pkispawn interactive installation failed

* Mon Feb 17 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.2-1
- Rebase to PKI 10.8.2
- Bug 1802006 - KRA installation failed to create ECC admin cert

* Mon Feb 10 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.1-1
- Rebase to PKI 10.8.1

* Fri Feb 07 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.0-1
- Rebase to PKI 10.8.0

* Thu Jan 16 2020 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.0-0.5
- Rebase to PKI 10.8.0-b3

* Fri Dec 13 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.0-0.4
- Rebase to PKI 10.8.0-b2

* Wed Dec 11 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.0-0.3
- Rebase to PKI 10.8.0-b1

* Fri Nov 22 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.0-0.2
- Rebase to PKI 10.8.0-a2

* Thu Oct 31 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.8.0-0.1
- Rebase to PKI 10.8.0-a1

* Wed Aug 14 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.7.3-1
- Rebase to PKI 10.7.3
- Bug 1698084 - pkidestroy not working as expected
- Bug 1468050 and Bug #1448235 - Support AES for LWCA key replication

* Tue Jul 23 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.7.2-1
- Rebase to PKI 10.7.2
- Bug 1721340 - TPS installation failure
- Bug 1248216 - Incorrect pkidaemon status
- Bug 1729215 - cert-fix: detect and prevent pkidbuser being used as --agent-uid
- Bug 1698059 - pki-core implements crypto

* Thu Jun 13 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.7.1-2
- Fix cloning issue
- Fix TPS installation issue

* Wed Jun 12 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.7.1-1
- Rebase to PKI 10.7.1

* Wed Apr 24 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.7.0-1
- Rebase to PKI 10.7.0

* Mon Jan 28 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.9-2
- Bug 1652269 - Replace Nuxwdog

* Mon Jan 14 2019 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.9-1
- Rebase to PKI 10.6.9
- Bug 1629048 - X500Name.directoryStringEncodingOrder overridden by CSR encoding
- Bug 1652269 - Replace Nuxwdog
- Bug 1656856 - Need Method to Include SKI in CA Signing Certificate Request

* Thu Nov 29 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.8-1
- Rebase to PKI 10.6.8
- Bug 1602659 - Fix issues found by covscan
- Bug 1566360 - Fix missing serial number from pki-server subsystem-cert-find

* Fri Oct 26 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.7-3
- Bug 1643101 - Fix problems due to token normalization

* Tue Oct 23 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.7-2
- Bug 1623444 - Fix Python KeyClient KeyRequestResponse parsing

* Fri Oct 05 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.7-1
- Rebase to PKI 10.6.7

* Fri Aug 24 2018 Alexander Bokovoy <abokovoy@redhat.com> 10.6.6-3
- Build on s390x

* Wed Aug 22 2018 Alexander Bokovoy <abokovoy@redhat.com> 10.6.6-2
- Use platform-python interpreter
- Bug 1620066 - pkispawn crashes as /usr/bin/python3 does not exist

* Mon Aug 13 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.6-1
- Rebase to PKI 10.6.6

* Wed Aug 08 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.5-1
- Rebase to PKI 10.6.5

* Tue Aug 07 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.4-4
- Bug 1612063 - Do not override system crypto policy (support TLS 1.3)

* Wed Aug 01 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.4-3
- Patch PKI to use Jackson 2 and avoid Jackson 1 dependency.
  Add direct dependency on slf4j-jdk14.

* Tue Jul 31 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.4-2
- Update Jackson and RESTEasy dependencies

* Fri Jul 20 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.4-1
- Rebase to PKI 10.6.4

* Thu Jul 05 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.3-1
- Rebase to PKI 10.6.3

* Mon Jul 02 2018 Miro Hrončok <mhroncok@redhat.com> 10.6.2-4
- Rebuild for Python 3.7

* Thu Jun 28 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.2-3
- Fix macro expressions
- Bug 1566606 - pki-core: Switch to Python 3
- Bug 1590467 - pki-core: Drop pylint dependency from RHEL 8

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> 10.6.2-2
- Rebuild for Python 3.7

* Fri Jun 15 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.2-1
- Rebase to PKI 10.6.2

* Wed May 30 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.1-3
- Update JSS dependency
- Update Tomcat dependency
- Fix rpmlint warnings

* Fri May 04 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.1-2
- Bug 1574711 - pki-tools cannot be installed on current Rawhide
- Fix rpmlint warnings

* Thu May 03 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.1-1
- Rebase to PKI 10.6.1
- Bug 1559047 - pki-core misses a dependency to pki-symkey
- Bug 1573094 - FreeIPA external CA installation fails

* Wed Apr 11 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.0-1
- Update project URL and package descriptions
- Clean up spec file
- Rebase to PKI 10.6.0 final

* Thu Mar 29 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.0-0.3
- Iryna Shcherbina <ishcherb@redhat.com>: Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)
- Rebase to PKI 10.6.0 beta2

* Thu Mar 15 2018 Red Hat PKI Team <rhcs-maint@redhat.com> 10.6.0-0.2
- Rebase to PKI 10.6.0 beta

